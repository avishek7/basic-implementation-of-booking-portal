'use strict';

/* Directives */

telstraGBNApp.directive('firstPage',function(){
	return {
		restrict:'A',
		templateUrl :'partials/FirstPage.html'
	};
});

telstraGBNApp.directive('secondPage',function(){
	return {
		restrict:'E',
		templateUrl :'partials/SecondPage.html'
	};
});

telstraGBNApp.directive('thirdPage',function(){
	return {
		restrict:'E',
		templateUrl :'partials/ThirdPage.html'
	};
});